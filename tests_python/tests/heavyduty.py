from launchers.sandbox import Sandbox
from tools import utils, constants
from time import sleep
from typing import Optional
import datetime
import random
import threading

_NUM_CLIENTS = 2
# Start at 1 bootstrap accounts start at boostrap1:
_ALL_CLIENTS = list(range(1, _NUM_CLIENTS + 1))
_TEST_TIME = 60 * 5  # in seconds

class ClientData:
    """
        Client with id 1 transfers
        while clients with id > 1 request baking rights

        I'd like to have more clients doing transfers,
        but the sandbox's API accepts at most a baker per node :-(
    """

    def __init__(self, sandbox: Sandbox, node, id):
        self.sandbox = sandbox
        self.node = node
        self.id: int = id

def thread_code(data: ClientData):
    sleep(2)
    starting_time = datetime.datetime.now()
    client = data.sandbox.get_new_client(node=data.node)
    assert data.id >= 1

    while True:
        now_time = datetime.datetime.now()
        timedelta = now_time - starting_time
        elapsed_time_seconds = timedelta.seconds
        if elapsed_time_seconds > _TEST_TIME:
            log(f"Quitting after {elapsed_time_seconds} seconds")
            break

        def log(msg: str):
            print(f"client #{data.id}, tick {elapsed_time_seconds}: " + msg)

        action = data.id
        assert 1 <= action and action <= 2
        if action == 3:  # get endorsing_rights
            path = "/chains/main/blocks/head/helpers/endorsing_rights"
            cmd_to_log = "rpc get " + path
            try:
                # log(f"STARTING: {cmd_to_log}")
                client.rpc("get", path)
                log(cmd_to_log)
            except Exception as exc:
                log(f"Failure: {cmd_to_log}")
                log("SKIPPING FAILURE:")
                # print(exc) Do not print (see below)
        if action == 2:  # get baking_rights
            path = "/chains/main/blocks/head/helpers/baking_rights?&all=true"
            cmd_to_log = "rpc get " + path
            try:
                # log(f"STARTING: {cmd_to_log}")
                client.rpc("get", path)
                log(cmd_to_log)
            except Exception as exc:
                log(f"Failure: {cmd_to_log}")
                log("SKIPPING FAILURE:")
                # print(exc) Do not print (see below)
        elif action == 1:  # transfer
            amount = random.randrange(1, 100)
            giver = f"bootstrap1"
            receiver = "bootstrap2"
            cmd_to_log = f"transfer {amount} from {giver} to {receiver}"
            try:
                # log(f"STARTING: {cmd_to_log}")
                client.transfer(str(amount), giver, receiver)
                log(cmd_to_log)
            except Exception as exc:
                log(f"Failure: {cmd_to_log}")
                log("SKIPPING FAILURE:")
                # print(exc) Do not print, it can make python crash as follows:
                # RecursionError: maximum recursion depth exceeded while getting the str of an object
        else:
            raise Exception(f"Wrong action number: {action}")
        sleep(1)  # Sleep 1 second


def test_bench_node(sandbox: Sandbox):
    sandbox.add_node(0, params=constants.NODE_PARAMS)
    node = sandbox.node(0)
    utils.activate_alpha(sandbox.client(0))
    sleep(5) # Let node get ready

    sandbox.client(0).bake("bootstrap1")
    sleep(2)

    sandbox.add_baker(0, 'bootstrap1',
                      proto=constants.ALPHA_DAEMON)
    sleep(2)

    assert _NUM_CLIENTS >= 1

    threads = []
    for i in _ALL_CLIENTS:
        data = ClientData(sandbox, node, i)
        threads.append(threading.Thread(target=thread_code, args=(data, )))

    i = 0
    while i < len(threads):
        threads[i].start()
        print(f"Started client #{i + 1}")
        i += 1

    i = 0
    while i < len(threads):
        threads[i].join()
        print(f"Joined client #{i + 1}")
        i += 1
