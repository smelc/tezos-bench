# tezos-bench

This repository contains scripts to benchmark the duration of:

* commands of `tezos-client`, and
* rpc answers of `tezos-node`.

The scripts are specific for comparing the proxy mode and the light mode
with the vanilla mode. However, they could be generalized to
track performance regression.

## Directory `tezt`

This directory's entrypoint is `tezt/main.py`. It should be executed
from a [`tezos`](https://gitlab.com/tezos/tezos/) git directory.

`main.py` supposes that `tezt/lib/client.ml` has been modified as follows:

```diff
diff --git a/tezt/lib/process.ml b/tezt/lib/process.ml
index 309147de38..04efaa72c6 100644
--- a/tezt/lib/process.ml
+++ b/tezt/lib/process.ml
@@ -179,18 +179,18 @@ let wait process =
      the exit status. *)
   if ID_map.mem process.id !live_processes then (
     live_processes := ID_map.remove process.id !live_processes ;
-    if process.log_status_on_exit then
+    if process.log_status_on_exit || true then
       match status with
       | WEXITED code ->
-          Log.debug "%s exited with code %d." process.name code
+          Log.debug "[%s] exited with code %d." process.name code
       | WSIGNALED code ->
           Log.debug
-            "%s was killed by signal %s."
+            "[%s] was killed by signal %s."
             process.name
             (show_signal code)
       | WSTOPPED code ->
           Log.debug
-            "%s was stopped by signal %s."
+            "[%s] was stopped by signal %s."
             process.name
             (show_signal code) ) ;
   return status
```

For the node, the script generates such histograms:

![tezos-node histogram](https://i.imgur.com/c2YswVz.png)

It compares the durations of rpc answers in `--mode client` vs durations
in `--mode light`. The abscissa consists of rpc paths.

For the client, the script generates such histograms:

![tezos-client histogram](https://i.imgur.com/OXme9Z5.png)

It compares the durations of calls to `tezos-client` in `--mode client`
vs durations in `--mode light`.

## Configuration

`tezt/main.py` takes as arguments a list of files. Each file is a configuration
describing a benchmark to do. Here is the configuration used for
benchmarking the [light mode](https://gitlab.com/tezos/tezos/-/merge_requests/2609)
([versioned here](https://gitlab.com/smelc/tezos-bench/-/blob/master/tezt/spec-mr-2609.json)):

```json
[
    {
        "name": "heavy duty",
        "reference": {
            "tezt_test": "heavy_duty vanilla (alpha)",
            "label": "--mode client"
        },
        "other": {
            "tezt_test": "heavy_duty light (alpha)",
            "label": "--mode light"
        },
        "clients": true,
        "nodes": true,
        "total": true
    }
]
```

Here is the meaning of the various fields:

* `name` is used for naming the created `png` files
* `reference` is the reference state. The `tezt_test` field indicates
  the tezt to execute (with tezt's `--test` flag) to obtain data on
  this state. The `label` field is used to create the legend in the created
  histograms. If omitted, `label` is defaulted to: `git commit title@git branch - git changeset`.
  The `tezt_args` field is used to pass custom arguments to `tezt`.
  It is a list of strings, which is defaulted to the empty list.
* `other` is the non-reference state. The possible fields are the
  same as the `reference` state. Usually it uses the same `tezt_test`
  as the other state, but not the same `label`.
* `clients` indicates whether to track the performance of `tezos-client`
  commands. If omitted it defaults to `true`.
* `nodes` indicates whether to track the performance of `tezos-node`
  rpcs. If omitted it defaults to `true`.
* `skip` is an optional Boolean field that indicates to skip this scenario,
  which defaults to `false`. This field is useful when debugging a specific scenario,
  to skip the others.
* `total` indicates whether to generate the `total` bars in the created
  histograms which sum up the durations of all commands/all rpcs. If omitted,
  it defaults to `false` (because having the total bars usually flatten the histograms,
  making it not super readable)

Here is the configuration used for
benchmarking [MR 2239](https://gitlab.com/tezos/tezos/-/merge_requests/2239)
([versioned here](https://gitlab.com/smelc/tezos-bench/-/blob/master/tezt/spec-mr-2239-smelc.json)):

```json
[
    {
        "name": "RPC_test",
        "reference": {
            "tezt_file": "RPC_test.ml",
            "label": "master"
        },
        "other": {
            "tezt_file": "RPC_test.ml",
            "label": "stream-json-rather-than-dumping-it",
            "cwd": "/home/churlin/dev/trezos"
        },
        "clients": true,
        "clients_filter": ".* rpc get .*",
        "nodes": true
    }
]
```

Here are the fields that are new:

* `tezt_file` specifies the file to pass to tezt's `--file` flag, in case
  you wanna execute a whole file.
* `cwd` specifies where to execute the tests. Use that to
  avoid having to `make` when you want to compare different commits.
  In my case, because I execute `main.py` in `/home/churlin/dev/tezos`;
  the `reference` state executes by default here and the `other` state
  executes in the t**r**ezos directory, which is configured differently.
  Not that alternatively, you can use a `git` field to specify the
  commit to use. This allows to use a single repo for comparisons, but
  it is usually slower, because `main.py` will do the correct `git checkout` and
  issue the required `make` calls; and `make` is slow.
* `clients_filter` specifies a python regexp to filter
  the `tezos-client` commands being shown in generated histograms. Use that
  if you're interested in a subset of the commands executes by your tests.
  Similarly, you can use `nodes_filter` to filter rpcs handled by `tezos-node`.

## Deprecated directories

Directories `client` and `nodes` are deprecated. They were used in Fall
2020 to generate data for
[the `--mode proxy` MR](https://gitlab.com/tezos/tezos/-/merge_requests/1943).
The recommended way is now to use `tezt`. Automation is better.

### Directory `client` (⚠️ deprecated ⚠️, prefer using `tezt`)

Directory `client` contains scripts to benchmark `tezos-client` when
executed with `pytest`:

* `time_py_client.patch` is the patch to instrument `tests_python` so
  that calls to `tezos-client` generate timing files. Apply it as follows
  (at tezos' root): `git apply time_py_client.patch`. Set `TZ_TIMING_KEY`
  beforehand to specify the prefix of generated timing files.
* `histogriphy.py` processes every timing file given as argument.
* `histogriphy.sh` generates timing files on branch `smelc-issue-154-proxy`
  and on its closest `master` parent.
* `bigclassify.py` finds files generated by `histogriphy.sh` and generates
  a comparison of timings from `smelc-issue-154-proxy` and `master` like this:
  ![proxy VS master tezos-client timings](https://i.imgur.com/MFPsgwz.png)

All these files must be put at the root of the
[tezos](https://gitlab.com/nomadic-labs/tezos) repository.

## Directory `node` (⚠️ deprecated ⚠️), prefer using `tezt`

Directory `node` contains scripts to benchmark `tezos-node` when executed
with `pytest`:

* `benchnode.py` executes `pytest`
  and analyses the logs produced so far to generated a comparison like the one
  shown below. Pass `--no-pytest` to skip executing `pytest`. Pass
  `--no-analysis` to skip producing the comparison. Other arguments
  are passed to `pytest` (for example to restrict the files to execute, as
  `benchnode.sh` does below). Also see the header for additional flags.

  The analysis keeps track of the duration of honoring the RPC requests.
  This is done by setting `TEZOS_LOG` to `rpc->debug` and processing the
  logs produced.
* `benchnode.sh` calls `benchnode.py`, specifying to execute
  [heavyduty.py](https://gitlab.com/smelc/tezos-bench/-/blob/master/tests_python/tests/heavyduty.py).
  It then generates a comparison of timings from `smelc-issue-154-proxy`
  and `master` like this:
  ![proxy VS master tezos-node timings](https://i.imgur.com/KEmsDIr.png)

  This graph shows the number of RPC requests honored (at the top of bars).
  The height of the bar shows the total time spent honoring the given RPC.

All these files must be put at the root of the
[tezos](https://gitlab.com/nomadic-labs/tezos) repository.
