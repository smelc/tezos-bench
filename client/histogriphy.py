#!/usr/bin/env python3

import os
import sys
import matplotlib.pyplot as plt

_DESTINATION = "proxy_bench"
_SCRIPT = sys.argv[0]

if len(sys.argv) == 1:
    print(f"{_SCRIPT} expects at least one argument", file=sys.stderr)
    sys.exit(1)

def _process_file(file_: str):
    """
        Reads a timing file and returns a list of str * int tuples
        where the first member is the tezos command and the second
        member is the mean of timings of this command. The returned
        list is sorted by increasing second members.
    """
    if not os.path.isfile(file_):
        print(f"{file_} doesn't exist", file=sys.stderr)
        sys.exit(1)
    result = {}
    with open(file_, 'r') as handle:
        # line_number = 0
        for line in handle.readlines():
            # try:
            line = line.strip()
            name_and_timings = line.split(" ")
            # print("\n".join(name_and_timings))
            assert len(name_and_timings) >= 2
            name = name_and_timings[0]
            assert name not in result
            timings = [int(x) for x in name_and_timings[1:]]
            assert timings
            mean = sum(timings) / len(timings)
            result[name] = mean
            # except Exception as exc:
            #     print(exc)
            #     print(f"Skipping line {line_number}", file=sys.stderr)
            # finally:
            #     line_number += 1
    result_ = list(result.items())
    result_.sort(key=lambda p: p[1])
    return result_

for file_ in sys.argv[1:]:
    data = _process_file(file_)
    # print("*" * (len(file_) + 4))
    # print(f"* {file_} *")
    # print("*" * (len(file_) + 4))
    # presentable = [f"{x}->{str(y)}" for (x, y) in data]
    # print("\n".join(presentable))

    proxy = "proxy" in file_
    color = "r" if proxy else "g"
    label = "--mode " + ("proxy" if proxy else "client")
    plt.plot(range(len(data)), [p[1] for p in data], color + "o", label=label)

plt.legend(loc='upper left')
plt.xlabel("tezos-client commands")
plt.ylabel("duration (milliseconds)")

DEST = _DESTINATION
NEXT_IDX = 0
while os.path.isfile(DEST + ".png"):
    # File exists already, choose another name
    DEST = f"{_DESTINATION}_{str(NEXT_IDX)}"
    NEXT_IDX += 1
DEST += ".png"
plt.savefig(DEST)
print(f"Written {DEST}")
