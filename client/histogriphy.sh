#!/usr/bin/env bash
# Script to generate performance comparison of the vanilla client
# and the client with proxying. This script supposes that the code
# augmentation to generate timings in the first stash.

PROXY_BRANCH="smelc-issue-154-proxy"
MASTER_REF="fb634c0c4ce860b61749244e8fd029f6a5d2a042"

[[ -e ".git/config" ]] || { echo "This script must be executed from tezos' git root"; exit 1; }

DIRTY=$(git diff-index --name-only HEAD --)
[[ -z "$DIRTY" ]] || { echo "repo has changes: please start from a clean repo"; exit 1; }

git checkout "$PROXY_BRANCH"

function execute() {
  [[ -n "$1" ]] || { echo "execute expects the identifier to checkout as first argument"; return 1; }
  git checkout "$1" || { echo "Cannot checkout $1"; return 1; }
  git apply time_py_client.patch || { echo "Cannot apply instrumenting patch"; return 1; }
  make || { git reset HEAD --hard; exit 1; }
  if [[ "$1" == "smelc-issue-154-proxy" ]]; then
    # Delete test which is NOT on master
    local -r TARGET="tests_python/tests/test_proxy.py"
    rm "$TARGET" || { echo "Cannot remove $TARGET"; return 1; }
  fi
  export TZ_TIMING_KEY="$1"
  (cd tests_python && poetry run pytest -x -q) || { echo "pytest failed"; RETURN_CODE=1; }
  unset TZ_TIMING_KEY
  git reset HEAD --hard
  if [[ -n "$RETURN_CODE" ]]; then
    return $RETURN_CODE
  fi
}

killall -9 tezos-node

execute "$PROXY_BRANCH" || exit "$?"
execute "$MASTER_REF" || exit "$?"

PROXY_DATA="/tmp/pytest_timings_${PROXY_BRANCH}"
MASTER_DATA="/tmp/pytest_timings_${MASTER_REF}"

# PROXY_LINES=$(wc -l $PROXY_DATA)
# MASTER_LINES=$(wc -l $MASTER_DATA)
# if [[ "$PROXY_LINES" != "$MASTER_LINES" ]]; then
#   "Proxy data has $PROXY_LINES lines while master data has $MASTER_LINES. Weird."
# fi

rm -Rf "proxy_bench.png"
./histogriphy.py "$PROXY_DATA" "$MASTER_DATA" || exit 1

DEST_DIR="proxy_bench/$(date '+%Y-%m-%d-%H-%M-%S')"
mkdir -p "$DEST_DIR" || exit 1
mv "proxy_bench.png" "$DEST_DIR/." || exit 1
mv /tmp/pytest_timings_* "$DEST_DIR/." || exit 1

./bigclassify.py "proxy_bench"
