#!/usr/bin/env python3
"""
    Find timing data in such files:

    pytest_timings_*

    A timing file is considered coming from the proxy if it contains the
    string "proxy"

    Classify all data by the commands being used and create
    another graph from that. Searches files by default in the
    current directory, give an argument to specify a search starting
    point
"""


from typing import Dict, List, Tuple
import glob
import matplotlib.pyplot as plt
import os
import sys


def _process_file(file_: str) -> Dict[str, List[int]]:
    """
        Reads a timing file and returns a dict of str * List[int] tuples
        where the first member is the tezos command and the second
        member is all timings of this command.
    """
    if not os.path.isfile(file_):
        print(f"{file_} doesn't exist", file=sys.stderr)
        sys.exit(1)
    result = {}
    with open(file_, 'r') as handle:
        line_number = 0
        for line in handle.readlines():
            # try:
            line = line.strip()
            name_and_timings = line.split(" ")
            # print("\n".join(name_and_timings))
            assert len(name_and_timings) >= 2
            name = name_and_timings[0]
            assert name not in result
            timings = [int(x) for x in name_and_timings[1:]]
            result[name] = timings
            line_number += 1
            # except Exception as e:
            #     print(e)
            #     print(f"Skipping line {line_number}", file=sys.stderr)
            # finally:
            #     line_number += 1
    return result


def _remove_flags(cmd: List[str]) -> List[str]:
    if len(cmd) <= 2:
        return cmd
    if cmd[0] in ["--protocol", "-w"]:
        return cmd[2:]
    return cmd


def _normalize_tezos_client_command(key: str) -> List[str]:
    """
        `key` is a prefix of a line in a `pytest_timings_*` file
        like:

        prepare_multisig_transaction_on_KT1JyHhZ2BhEss1UiWMx1Jf1FwP7eSpZfjde_setting_threshold_to_2_and_public_keys_to_foo_boo_--bytes-only
        sign_bytes_0x05070707070a000000047a06a7700a0000001601540d6ddc1f288a7955a74137a11a3ffdb792a2c70007070000050507070080dac4090a000000160000e7670f32038107a59a2b9cfefae36ea21f5aa63c_for_bar
        -block_genesis_activate_protocol_ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK_with_fitness_1_and_key_activator_and_parameters_/tmp/tmpk2wc9bmx_--timestamp_2020-08-19T15

        This function returns a human understandble tezos-client command
        from such lines, as a list of strings.
    """
    elems = _remove_flags(key.split("_"))
    assert elems
    def _contains_all(names: List[str]):
        for name in names:
            if not name in elems:
                return False
        return True
    starters = [["activate", "account"],
                ["add", "address"],
                ["bake", "for"],
                ["bootstrapped"],
                ["call"],
                ["config", "update"],
                ["convert", "data"],
                ["convert", "script"],
                ["compute", "chain", "id"],
                ["deploy"],
                ["endorse"],
                ["expand", "macros"],
                ["get", "a"],
                ["get", "b"],
                ["get", "balance"],
                ["get", "contract", "entrypoint"],
                ["get", "contract", "storage"],
                ["get", "delegate", "for"],
                ["hash", "data"],
                ["list", "known", "addresses"],
                ["originate", "contract"],
                ["prepare"],
                ["remember", "contract"],
                ["remember", "script"],
                ["run", "script"],
                ["run", "transaction"],
                ["rpc", "post"],
                ["set", "delegate"],
                ["show", "known", "contract"],
                ["sign"],
                ["show", "address"],
                ["show", "voting", "period"],
                ["submit", "ballot"],
                ["submit", "proposals"],
                ["typecheck", "data"],
                ["typecheck", "script"],
                ["wait", "for"],
                ["withdraw", "delegate"],
               ]
    # More precise first
    for starter in starters:
        if len(elems) >= len(starter) and elems[0:len(starter)] == starter:
            return starter
    normals = [["bake"],
               ["config", "init"],
               ["config", "show"],
               ["create", "mockup"],
               ["genesis", "activate", "protocol"],
               ["gen", "keys"],
               ["import", "secret", "key"],
               ["increment"],
               ["list", "known", "addresses"],
               ["list", "mockup", "protocols"],
               ["list", "understood", "protocols"],
               ["none", "config", "update"],
               ["rpc", "get"],
               ["rpc", "post"],
               ["transfer"],
              ]
    # More fuzzy then
    for normal in normals:
        if _contains_all(normal):
            return normal
    raise Exception(f"Cannot normalize key: {key} (elems: {str(elems)})")


def _classify(data: Dict[str, List[int]]) -> Dict[str, List[int]]:
    result: Dict[str, List[int]] = {}
    for (k, ints) in data.items():
        k_prime = " ".join(_normalize_tezos_client_command(k))
        if k_prime in result:
            # Append
            result[k_prime] = result[k_prime] + ints
        else:
            # Initialize
            result[k_prime] = ints
    return result


def _check_data_consistency(file1_: str, data1: Dict[str, List[int]],
                            file2_: str, data2: Dict[str, List[int]]):
    """ Check keys are the same and the number of timings is similar """
    data1_keys = list(data1.keys())
    data2_keys = list(data2.keys())
    missing_in_2 = [x for x in data1_keys if x not in data2_keys]
    if missing_in_2:
        print(f"{file1_} has more keys than {file2_}:", file=sys.stderr)
        for missing in missing_in_2:
            print(missing, file=sys.stderr)
        sys.exit(1)
    missing_in_1 = [x for x in data2_keys if x not in data1_keys]
    if missing_in_1:
        print(f"{file2_} has more keys than {file1_}:", file=sys.stderr)
        for missing in missing_in_1:
            print(missing, file=sys.stderr)
        sys.exit(1)
    for k in data1.keys():
        nb_timings_1 = len(data1[k])
        nb_timings_2 = len(data2[k])
        if nb_timings_1 != nb_timings_2:
            print("WARNING")
            print(f"Files {file1_} and {file2_} have different number of timings for key {k}:")
            print(f"{nb_timings_1} != {nb_timings_2}")


_SEARCH_START = "." if len(sys.argv) <= 1 else sys.argv[1]

if not os.path.isdir(_SEARCH_START):
    print(f"{_SEARCH_START} should be a directory", file=sys.stderr)
    sys.exit(1)

_PATHS = glob.glob(f"{_SEARCH_START}/**/pytest_timings*", recursive=True)
_PATHS = [str(x) for x in _PATHS]

if not _PATHS:
    print("No pytest_timings* file found!", file=sys.stderr)
    sys.exit(1)

print(f"Found {len(_PATHS)} files:")
for path in _PATHS:
    print("  " + path)

_PROXY_FILES = [x for x in _PATHS if "proxy" in os.path.basename(x)]
if not _PROXY_FILES:
    print("No proxy data found!")
    sys.exit(1)
_MASTER_FILES = [x for x in _PATHS if x not in _PROXY_FILES]
if not _MASTER_FILES:
    print("No master data found!")
    sys.exit(1)

_PROXY_DATA = [(x, _classify(_process_file(x))) for x in _PROXY_FILES]
_MASTER_DATA = [(x, _classify(_process_file(x))) for x in _MASTER_FILES]

for (f, d) in _PROXY_DATA[1:] + _MASTER_DATA:
    _check_data_consistency(_PROXY_DATA[0][0], _PROXY_DATA[0][1], f, d)

def _aggregate_data(data: List[Dict[str, List[int]]], _name: str)-> List[Tuple[str, float]]:
    big_dict: Dict[str, List[int]] = {}  # Please do not make a typo
    for small_dict in data:
        for (k, ints) in small_dict.items():
            if k in big_dict:
                # Extend
                big_dict[k] = big_dict[k] + ints
            else:
                # Initialize
                big_dict[k] = ints
    res = []
    for (k, ints) in big_dict.items():
        nb_timings = 676 if k == "rpc get" else len(ints)
        mean = sum(ints) / nb_timings
        key = f"{k} ({nb_timings})"
        res.append((key, mean))
    res.sort(key=lambda p: p[1])
    # To debug data:
    # print(f"{_name} data")
    # for (k, v) in res:
    #     print(f"  {k}->{str(v)}")
    return res

_PROXY_AGGREGATED_DATA = _aggregate_data([x[1] for x in _PROXY_DATA], "proxy")
_MASTER_AGGREGATED_DATA = _aggregate_data([x[1] for x in _MASTER_DATA], "master")

def _plot_data(label: str, color: str, alpha: float, data: List[Tuple[str, float]]):
    names = [p[0] for p in data]
    values = [p[1] for p in data]
    plt.plot(names, values, color + "o", label=label, alpha=alpha)

plt.figure(figsize=(8, 8))
plt.subplots_adjust(bottom=0.33) # Make bottom area larger
_plot_data("--mode proxy", "r", 0.5, _PROXY_AGGREGATED_DATA)
_plot_data("--mode client", "g", 0.5, _MASTER_AGGREGATED_DATA)

plt.legend(loc='upper left')
plt.xlabel("tezos-client commands")
plt.xticks(rotation=90)
plt.ylabel("duration (milliseconds)")

BASE_DEST = "classified"
DEST = BASE_DEST
NEXT_IDX = 0
while os.path.isfile(DEST + ".png"):
    # File exists already, choose another name
    DEST = f"{BASE_DEST}_{str(NEXT_IDX)}"
    NEXT_IDX += 1
DEST += ".png"
plt.savefig(DEST)
print(f"Written {DEST}")
