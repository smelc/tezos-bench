#!/usr/bin/env bash
#
# Script to performance of the node, comparing when the client is in
# proxy mode or not.

PROXY_BRANCH="smelc-issue-154-proxy-default-mode"
MASTER_REF="fb634c0c4ce860b61749244e8fd029f6a5d2a042"

[[ -e ".git/config" ]] || { echo "This script must be executed from tezos' git root"; exit 1; }

DIRTY=$(git diff-index --name-only HEAD --)
[[ -z "$DIRTY" ]] || { echo "repo has changes: please start from a clean repo"; exit 1; }

git checkout "$PROXY_BRANCH"

# LOG_SUFFIX="bson"
# LOG_RE='.*5min_proxy_kiss.*'

function execute() {
  [[ -n "$1" ]] || { echo "execute expects the identifier to checkout as first argument"; return 1; }
  git checkout "$1" || { echo "Cannot checkout $1"; return 1; }
  make || { echo "Cannot make"; return 1; }
  CMD="./benchnode.py tests/heavyduty.py -s -x --no-analysis"
  [[ -z "$LOG_SUFFIX" ]] || { CMD+=" --log-suffix"; CMD+=" $LOG_SUFFIX"; }
  echo "$CMD"
  $CMD || { RETURN_CODE="$?"; echo "benchnode.py failed"; }
  if [[ -n "$RETURN_CODE" ]]; then
    return $RETURN_CODE
  fi
}

killall -9 tezos-node  # Defense against bad previous state
export TEZOS_LOG="rpc -> debug"
execute "$PROXY_BRANCH" || exit "$?"
execute "$MASTER_REF" || exit "$?"
unset TEZOS_LOG

rm -Rf "benchnode.png"
CMD="./benchnode.py --no-pytest"
[[ -z "$LOG_RE" ]] || { CMD+=" --log-re"; CMD+=" $LOG_RE"; }
echo "$CMD"
$CMD || exit 1