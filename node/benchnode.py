#!/usr/bin/env python3
"""
    Script that executes pytest (from within the tests_python directory),
    collects logs of the node, and generate
    a distribution of the timings of RPC calls for all logs generated so far
    (those of this run, and previous ones too).

    Arguments:
      --clean makes this script deletes the logs upon leaving
      --debug makes this script verbose
      --no-analysis makes this script solely execute pytest
      --no-pytest makes this script skip executing pytest

    All other arguments are passed to pytest.
"""

from typing import Any, Dict, List, Tuple
import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import shutil
import subprocess
import sys

_IDENTIFIER = subprocess.run(["git", "rev-parse", "HEAD"],  # commit hash
                             capture_output=True, text=True).stdout.strip()
assert _IDENTIFIER
_BRANCH_NAME = subprocess.run(["git", "branch", "--show-current"],
                              check=False, capture_output=True,
                              text=True).stdout.strip()  # Take branch name
if _BRANCH_NAME:
    _IDENTIFIER += f"_{_BRANCH_NAME}"  # Add branch name if any

# Prefix of all log files ever
_ROOT = f"{os.getcwd()}/benchnode/pytest_benchnode_timings_"
# Directory of log files produced in this run (if any)
_BASE = f"{_ROOT}{_IDENTIFIER}"
_DEST_DIR = "node_bench"
_ARGS = list(sys.argv[1:])

_CLEAN = "--clean" in _ARGS
if _CLEAN:
    _ARGS = [x for x in _ARGS if x != "--clean"]
_DEBUG = "--debug" in _ARGS
if _DEBUG:
    _ARGS = [x for x in _ARGS if x != "--debug"]
_NO_ANALYSIS = "--no-analysis" in _ARGS
if _NO_ANALYSIS:
    _ARGS = [x for x in _ARGS if x != "--no-analysis"]
_NO_PYTEST = "--no-pytest" in _ARGS
if _NO_PYTEST:
    _ARGS = [x for x in _ARGS if x != "--no-pytest"]
_LOG_SUFFIX = None  # How to suffix the directories in which logs are stored
# Use that in conjunction to --log-re (below) to generated custom named
# logs and use --log-re to only analyse such files.
for i in range(len(_ARGS)):
    if _ARGS[i] == "--log-suffix":
        if i == len(_ARGS) - 1:
            print("--log-suffix expects an argument", file=sys.stderr)
            sys.exit(1)
        _LOG_SUFFIX = _ARGS[i+1]
        _ARGS.pop(i)
        _ARGS.pop(i)
        print(f"Recognized --log-suffix: {_LOG_SUFFIX}")
        break
_LOG_RE = None  # The regexp that log filepaths must match to be considered
for i in range(len(_ARGS)):
    if _ARGS[i] == "--log-re":
        if i == len(_ARGS) - 1:
            print("--log-re expects an argument", file=sys.stderr)
            sys.exit(1)
        _LOG_RE = _ARGS[i+1]
        _ARGS.pop(i)
        _ARGS.pop(i)
        print(f"Recognized --log-re: {_LOG_RE}")
        break

def _find_pytest_log_dir() -> str:
    idx = 0
    while True:
        tentative = _BASE
        if _LOG_SUFFIX:
            tentative += f"_{_LOG_SUFFIX}"
        tentative += f"_{idx}"
        if os.path.isdir(tentative):
            # Directory is taken already
            idx += 1
        else:
            # This directory is good
            os.makedirs(tentative)
            return tentative


def _run_pytest(log_dir: str):
    cmd = ["poetry", "run", "pytest", f"--log-dir={log_dir}"] + _ARGS
    if not os.path.isdir("tests_python"):
        print("tests_python directory not found")
        sys.exit(1)
    process_env = os.environ.copy()
    key = "TEZOS_LOG"
    # Note that "rpc->debug" suffices
    process_env[key] = "rpc->debug" # ; alpha.proxy_rpc->debug; 006-PsCARTHA.proxy_rpc->debug; proxy_getter->debug"
    print(f"Setting {key} to {process_env[key]}")
    print("tests_python> " + " ".join(cmd))
    completed_process = subprocess.run(cmd, cwd="tests_python",
                                       env=process_env, check=False,
                                       text=True, capture_output=True)
    if completed_process.returncode != 0:
        print("pytest failed", file=sys.stderr)

    def write_output(kind: str, output: str):
        if not output:
            return
        i = 0
        while True:
            filepath = os.path.join(log_dir, f"pytest_{kind}_{i}")
            if not os.path.isfile(filepath):
                break  # Found it
            i += 1
        with open(filepath, 'w') as handle:
            handle.write(output)
            print(f"Written {filepath}")

    write_output("stdout", completed_process.stdout)
    write_output("stderr", completed_process.stderr)


_RUN_LOG_DIR = None
if not _NO_PYTEST:
    _RUN_LOG_DIR = _find_pytest_log_dir()
    _run_pytest(_RUN_LOG_DIR)

if _NO_ANALYSIS:
    print("--no-analysis specified, exiting")
    sys.exit(0)

_LOG_FILES = glob.glob(f"{_ROOT}*/**", recursive=True)
_LOG_FILES = [x for x in _LOG_FILES
              if not _LOG_RE or re.match(_LOG_RE, x)]
_LOG_FILES = [str(x)
              for x in _LOG_FILES
              if os.path.isfile(x) and "node" in os.path.basename(x)]

if not _LOG_FILES:
    print("Found no log file")
    sys.exit(1)

summary = subprocess.run(["./summary.py", "benchnode"], capture_output=True, text=True,
                         check=True)
if summary.stdout.strip():
    print(summary.stdout.strip())
if summary.stderr.strip():
    print(summary.stderr.strip(), file=sys.stderr)

def _analyse_log_file(fpath: str, debug: bool) -> List[Tuple[str, List[float]]]:
    """
        Given the path of a log file, returns a list of pairs. The pair's
        first member is the rpc request. The pair's second member
        is a list of duration of this request; in milliseconds.
    """
    re_start = r"(.*) - rpc: \((\w+)\) receive request to (.*)"
    re_end = r"^(.*) - rpc: connection closed (\w+)"

    def log(msg: str):
        if debug:
            print(msg)

    log(f"Analysing {fpath}")
    result: Dict[int, int] = {}
    # RPCs whose starting line has been parsed, but the endline hasn't been
    # yet. The key is the request number while the value is a pair of a
    # datetime and the RPC being called.
    opened: Dict[int, Any] = {}
    # Format to match for example "Aug 21 10:56:31.925"
    date_format = "%b %d %H:%M:%S.%f"
    with open(fpath, 'r') as handle:
        line_number = -1
        for line in handle.readlines():
            line_number += 1
            # log(f"Analyzing {fpath} line {line_number}")
            line = line.strip()
            match_start = re.match(re_start, line)
            if match_start:
                start_date = match_start[1]
                start_request = int(match_start[2])
                start_rpc = match_start[3]
                if start_request in opened:
                    raise Exception(f"RPC request {start_request} started twice in {fpath}")
                start_date = datetime.datetime.strptime(start_date,
                                                        date_format)
                # log(f"Opening {start_request}: {start_rpc}")
                opened[start_request] = (start_date, start_rpc)
                continue
            match_end = re.match(re_end, line)
            if match_end:
                end_date = match_end[1]
                end_request = int(match_end[2])
                if end_request not in opened:
                    raise Exception(f"RPC request {end_request} ended before having started in {fpath}")
                (start_date, rpc_request) = opened.pop(end_request)
                end_date = datetime.datetime.strptime(end_date,
                                                      date_format)
                duration = (end_date - start_date).microseconds / 1000
                result[rpc_request] = (result[rpc_request]
                                       if rpc_request in result
                                       else []) + [duration]
                # log(f"Closing {end_request}: {duration}")
    if not result:
        raise Exception(f"No RPC timing found in {fpath}")
    for k in result.keys():
        result[k].sort()
    result = list(result.items())
    result.sort(key=lambda p: p[0])
    for (k, v) in result:
        vs = " ".join([str(x) for x in v])
        log(f"{k} -> {vs}")
    return result

if _CLEAN and _RUN_LOG_DIR:
    print(f"rm {_RUN_LOG_DIR}")
    shutil.rmtree(_RUN_LOG_DIR)

_ANALYSES = [(f, _analyse_log_file(f, _DEBUG)) for f in _LOG_FILES]
_PROXY_BRANCH="smelc-issue-154-proxy-default-mode"
_PROXY_ANALYSES = [p for p in _ANALYSES if _PROXY_BRANCH in p[0]]
_MASTER_ANALYSES = [p for p in _ANALYSES if _PROXY_BRANCH not in p[0]]
print(f"Found {len(_PROXY_ANALYSES) + len(_MASTER_ANALYSES)} files")
if len(_PROXY_ANALYSES) != len(_MASTER_ANALYSES):
    len_p = len(_PROXY_ANALYSES)
    len_m = len(_MASTER_ANALYSES)
    assert len_p != len_m
    len_diff = abs(len_p - len_m)
    assert len_diff > 0
    print(f"WARNING: Found {len_p} proxy but {len_m} master files")
    more = "proxy" if len_p > len_m else "master"
    print(f"WARNING: Hence ignoring {len_diff} {more} files")
    if len_p > len_m:
        _PROXY_ANALYSES = _PROXY_ANALYSES[:len_m]
    else:
        _MASTER_ANALYSES = _MASTER_ANALYSES[:len_p]
assert len(_PROXY_ANALYSES) == len(_MASTER_ANALYSES)
for (kind, analyses) in [("proxy", _PROXY_ANALYSES), ("master", _MASTER_ANALYSES)]:
    for filepath in [p[0] for p in analyses]:
        print(f"  {filepath} ({kind})")
_PROXY_ANALYSES = [p[1] for p in _PROXY_ANALYSES]
_MASTER_ANALYSES = [p[1] for p in _MASTER_ANALYSES]

def _merge_analyses(analyses: List[List[Tuple[str, List[float]]]],
                    debug:bool) -> List[Tuple[str, List[float]]]:
    d = {}
    for analysis in analyses:
        for (k, floats) in analysis:
            d[k] = d.get(k, []) + floats
    result = [(k, sorted(d[k])) for k in d.keys()]
    result.sort(key=lambda p: p[0])
    if debug:
        for (k, v) in result:
            assert all([isinstance(x, float) for x in v])
            vs = " ".join([str(x) for x in v])
            print(f"{k} -> {vs}")
    return result

def _normalize_rpc(rpc: str) -> str:
    """
        Normalize RPC paths, to merge data of specific RPC calls; so
        that they are gathered in the same x data
    """
    if re.match(r"/chains/\w+/blocks$", rpc):
        return "/chains/<chain_id>/blocks"
    if re.match(r"/chains/\w+/chain_id$", rpc):
        return "/chains/<chain_id>/chain_id"
    if re.match(r"/describe/.*", rpc):
        return "/describe"
    for suffix in ["mempool/monitor_operations",
                   "mempool/pending_operations",
                   "mempool/request_operations"]:
        if re.match(r"/chains/\w+/" + suffix, rpc):
            return "/chains/<chain_id>/" + suffix
    for suffix in ["context/constants",
                   "context/contracts",
                   "context/raw/bytes",
                   "live_blocks",
                   "minimal_valid_time",
                   "hash",
                   "header",
                   "header/shell",
                   "helpers/baking_rights",
                   "helpers/current_level",
                   "helpers/forge/operations",
                   "helpers/levels_in_current_cycle",
                   "helpers/preapply/block",
                   "helpers/preapply/operations",
                   "helpers/scripts/run_operations",
                   "protocols"]:
        if re.match(r"/chains/\w+/blocks/\w+/" + suffix, rpc):
            return "/chains/<chain_id>/blocks/<block_id>/" + suffix
    if re.match(r"/chains/\w+/blocks/\w+/context/nonces/\d+", rpc):
        return "/chains/<chain_id>/blocks/<block_id>/context/nonces"
    normals = ["/chains/main/blocks/head/hash",
               "/chains/main/blocks/head/header",
               "/chains/main/blocks/head/helpers/current_level",
               "/chains/main/blocks/head/helpers/scripts/run_operation",
               "/chains/main/mempool/pending_operations",
               "/config/network/user_activated_upgrades",
               "/describe/network/version",
               "/injection/block",
               "/injection/operation",
               "/monitor/bootstrapped",
               "/monitor/heads/main",
               "/network/version",
               "/version" ]
    if rpc not in normals:
        raise Exception(f"Unexpected normal rpc in normalize_rpc: {rpc}. Add it to normals if normal already, or extend the matchers above.")
    # Expected not to be affected
    return rpc

def _normalize_rpcs(d) -> List[Tuple[str, Any]]:
    """
        Applies _normalize_rpc to keys. Values can be anything.
    """
    result = {}
    assert isinstance(d, dict) or isinstance(d, list)
    for (k, v) in (d.items() if isinstance(d, dict) else d):
        k_norm = _normalize_rpc(k)
        result[k_norm] = result.get(k_norm, []) + v
    return list(result.items())

def _reduce_analysis_values(analysis: List[Tuple[str, List[float]]])\
    -> List[Tuple[str, float]]:
    """
        Return, for each RPC, the mean of its durations.

        The result is sorted according to the durations.
    """
    rpc_to_floats = {}
    for (rpc, floats) in analysis:
        rpc_to_floats[rpc] = rpc_to_floats.get(rpc, []) + floats
    result = [(rpc, sum(rpc_to_floats[rpc])) for rpc in rpc_to_floats]
    result.sort(key=lambda p: p[1])
    return result

_PROXY_DATA: List[Tuple[str, List[float]]] = _merge_analyses(_PROXY_ANALYSES, _DEBUG)
_MASTER_DATA: List[Tuple[str, List[float]]] = _merge_analyses(_MASTER_ANALYSES, _DEBUG)

_PROXY_DATA: List[Tuple[str, List[float]]]  = _normalize_rpcs(_PROXY_DATA)
_PROXY_DATA = sorted(_PROXY_DATA, key=lambda p: sum(p[1]))
_MASTER_DATA: List[Tuple[str, List[float]]]  = _normalize_rpcs(_MASTER_DATA)
_MASTER_DATA = sorted(_MASTER_DATA, key=lambda p: sum(p[1]))

_MIXED_DATA: List[Tuple[str, Tuple[List[float], List[float]]]] = []
for rpc in set([p[0] for p in _PROXY_DATA] + [p[0] for p in _MASTER_DATA]):
    proxy_dict = dict(_PROXY_DATA)
    master_dict = dict(_MASTER_DATA)
    pdata = proxy_dict.get(rpc, [])
    mdata = master_dict.get(rpc, [])
    _MIXED_DATA.append((rpc, (list(pdata), list(mdata))))

_MIXED_DATA.sort(key=lambda pp: sum(pp[1][0] + pp[1][1]))

_LABELS = [p[0] for p in _MIXED_DATA]
print(sorted(_LABELS))
_X = np.arange(len(_LABELS))
_WIDTH = 0.35

fig, ax = plt.subplots(figsize=(12, 12)) # Make figure larger
_PROXY_DURATIONS = [sum(floats) for floats in [p[1][0] for p in _MIXED_DATA]]
_MASTER_DURATIONS = [sum(floats) for floats in [p[1][1] for p in _MIXED_DATA]]
assert len(_LABELS) == len(_PROXY_DURATIONS)
assert len(_LABELS) == len(_MASTER_DURATIONS)
print(_PROXY_DURATIONS)
print(_MASTER_DURATIONS)
proxy_rects = ax.bar(_X - _WIDTH / 2,
                     _PROXY_DURATIONS,
                     _WIDTH, label="--mode proxy")
master_rects = ax.bar(_X + _WIDTH / 2,
                      _MASTER_DURATIONS,
                      _WIDTH, label="--mode client")

ax.set_ylabel("sum of tezos-node RPC answers durations (milliseconds)")
ax.set_title("sum of tezos-node RPC answers durations (bar)\nnumber of RPC calls (atop)")
ax.set_xticks(_X)
ax.set_xticklabels(_LABELS, rotation=90)
ax.legend()

def autolabel(rects, proxy_or_master: bool):
    """Attach a text label above each bar in *rects*."""
    i = 0
    for rect in rects:
        data: List[float] = _MIXED_DATA[i][1][0 if proxy_or_master else 1]
        text = str(len(data))
        ax.annotate(text,
                    xy=(rect.get_x() + rect.get_width() / 2, rect.get_height()),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        i += 1


autolabel(proxy_rects, True)
autolabel(master_rects, False)

fig.tight_layout()
plt.subplots_adjust(bottom=0.4) # Make bottom area larger

i = 0
while True:
    _IMG_FILEPATH = f"benchnode_{i}.png"
    if not os.path.isfile(_IMG_FILEPATH):
        break  # Found filename
    i += 1

# Yields a figure like this one:
# https://i.imgur.com/5ViLMa8.png
plt.savefig(_IMG_FILEPATH)
print(f"Written {_IMG_FILEPATH}")