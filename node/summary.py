#!/usr/bin/env python
"""
    Given a list of directories, for every directory, search
    for pytest_stdout* files and print a summary for each such file.

    If no directory is given, search `.`
"""

from typing import Dict, List, Tuple
import glob
import os
import re
import sys

_EVENTS = ["transfer", "rpc get"]

_STARTING_POINTS = sys.argv[1:]
if not _STARTING_POINTS:
    _STARTING_POINTS = ["benchnode"]

_LOG_FILES = []
for starting_point in _STARTING_POINTS:
    _LOG_FILES += glob.glob(f"{starting_point}/**/pytest_stdout*", recursive=True)

if not _LOG_FILES:
    print("No pytest_stdout* file found")
    sys.exit(0)

class Data:
    """ The number of events parsed from a pytest_stdout* file """

    def __init__(self):
        # Dict from event to number of successes and number of failures
        self.events: Dict[str, Tuple[int, int]] = {}

    def add(self, event: str, success_or_failure: bool):
        if event not in self.events:
            self.events[event] = (1 if success_or_failure else 0,
                                  0 if success_or_failure else 1)
        else:
            nb_successes = self.events[event][0]
            nb_failures = self.events[event][1]
            if success_or_failure:
                nb_successes += 1
            else:
                nb_failures += 1
            self.events[event] = (nb_successes, nb_failures)

    def get_number(self, event: str, success_or_failure: bool) -> int:
        pair = self.events.get(event, None)
        if pair is None:
            return 0
        i = 0 if success_or_failure else 1
        return pair[i]

    def size(self) -> int:
        """ The number of events recognized """
        result = 0
        for value in self.events.values():
            result += value[0]
            result += value[1]
        return result

    def __str__(self) -> str:
        result = ""
        for key in sorted(list(self.events.keys())):
            if result:
                result += "\n"
            value = self.events[key]
            result += f"{key}-> {value[0]}, {value[1]}"
        return result


def analyse_file(filepath: str) -> Data:
    assert os.path.isfile(filepath)
    data = Data()
    with open(filepath, 'r') as handle:
        line_nb = -1
        for line in handle.readlines():
            line_nb += 1
            line = line.strip()
            # All this could be optimized, KISS for now
            if not re.match(r"^client #\d, tick \d+", line):
                continue

            if re.match(r"^client #\d, tick \d+: SKIPPING FAILURE", line):
                continue
            if re.match(r"^client #\d, tick \d+: Quitting after \d+ seconds", line):
                continue

            recognized = False

            for event in _EVENTS:
                if re.match(r"^client #\d, tick \d+: " + event, line):
                    data.add(event, True)  # Add success
                    recognized = True
                    break

            if recognized:
                continue

            for event in _EVENTS:
                if re.match(r"^client #\d, tick \d+: Failure: " + event, line):
                    data.add(event, False)  # Add success
                    recognized = True
                    break

            if not recognized:
                raise Exception(f"Unrecognized event in line {line_nb} of {filepath}: {line}")
    return data

_PROXY_FILES = [p for p in _LOG_FILES if "smelc-issue-154-proxy" in p]
_MASTER_FILES = [p for p in _LOG_FILES if p not in _PROXY_FILES]


def order_files(filepaths) -> List[str]:
    """
        Order filepaths by the name of their parents directory. So that:

benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_1/pytest_stdout_0
benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_0/pytest_stdout_0
benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_2/pytest_stdout_0

        becomes:

benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_0/pytest_stdout_0
benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_1/pytest_stdout_0
benchnode/pytest_benchnode_timings_smelc-issue-154-proxy_2/pytest_stdout_0
    """
    def extract_key(filepath: str) -> str:
        dirname = os.path.dirname(filepath)
        if not dirname:
            return filepath
        elems = dirname.split("/")
        return elems[len(elems) - 1]
    return sorted(filepaths, key=lambda x: extract_key(x))


_PROXY_FILES = order_files(_PROXY_FILES)
_MASTER_FILES = order_files(_MASTER_FILES)

if len(_PROXY_FILES) != len(_MASTER_FILES):
    print(f"Found {len(_PROXY_FILES)} proxy but {len(_MASTER_FILES)} master files", file=sys.stderr)
    sys.exit(1)

_PAIRS = list(zip(_MASTER_FILES, _PROXY_FILES))

print(f"Found {len(_PAIRS) * 2} files:")
for p in _PAIRS:
    print(f"  {p[0]}\n  {p[1]}")

for p in _PAIRS:
    print(f"Comparing {p[0]} and\n          {p[1]}:")
    d0 = analyse_file(p[0])
    d0_lines = ["key -> successes, failures"] + str(d0).split("\n")
    d1 = analyse_file(p[1])
    d1_lines = ["key -> successes, failures"] + str(d1).split("\n")
    longest_line: int = max([len(x) for x in d0_lines + d1_lines])
    def pp(dx_lines):
        for dx_line in dx_lines:
            start = "* " + dx_line
            right_padding = " " * (longest_line + 3 - len(start))
            print(start + right_padding + "*")
    print("*" * (longest_line + 4))
    pp(d0_lines)
    print("*" * (longest_line + 4))
    pp(d1_lines)
    print("*" * (longest_line + 4))
