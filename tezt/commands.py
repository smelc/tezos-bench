import re
from typing import Dict, List, Tuple, Any

import rpc


def _normalize(cmd: str, root: str = None) -> str:
    """
    Normalize tezos-client commands, to classify x data
    """
    if not isinstance(cmd, str):
        print(str(type(cmd)))
        assert False
    root = root if root else cmd
    for one_arg_flag in [
        "--base-dir",
        "--endpoint",
        "--mode",
        "--protocol",
        "--sources",
        "--wait",
    ]:
        if m := re.match(f"{one_arg_flag} \S+ (.*)", cmd):
            return _normalize(m[1], root)
    # Delegations to rpc.normalize
    if m := re.match("rpc (\S+) '?(.*)'?", cmd):
        return f"rpc {m[1]} {rpc.normalize(m[2])}"
    if m := re.match('rpc (\S+) "(.*)"', cmd):
        return f"rpc {m[1]} {rpc.normalize(m[2])}"
    # Terminal cases
    if re.match("transfer \d+ from \S+ to \S+", cmd):
        return "transfer"
    for prefix in [
        "activate protocol",
        "bake for",
        "connect address",
        "create mockup",
        "gen keys",
        "import secret key",
        "originate contract",
        "remember contract",
        "set delegate",
        "show address",
        "submit ballot",
        "submit proposals",
    ]:
        if m := re.match(f"{prefix}.*", cmd):
            return prefix
    raise Exception(
        f"""Unrecognized tezos-client command: {cmd} (input command being {root})
If it starts with a flag, add it to the first regexps above in commands.py.
If it's a root command, add it to the last regexps above in commands.py."""
    )


def normalize_many(d) -> List[Tuple[str, Any]]:
    """
    Applies _normalize_command to keys. Values can be anything.
    """
    result: Dict[str, Any] = {}
    assert isinstance(d, dict) or isinstance(d, list)
    for (k, v) in d.items() if isinstance(d, dict) else d:
        assert k
        assert v
        k_norm = _normalize(k)
        assert k_norm
        result[k_norm] = result.get(k_norm, []) + v
    return list(result.items())
