#!/usr/bin/env python3
"""
Supposing that repos tezos-bench and tezos are siblings, execute as follows
from tezos/:

./../tezos-bench/tezt/main.py ./../tezos-bench/tezt/spec-mr-2609.json  # for light mode bench
"""

import argparse
import os
import re
import subprocess
import sys
from typing import Dict, List, Tuple

import analyser
import commands
import draw
import git
import parser as myparser
import rpc

parser = argparse.ArgumentParser(description="Benchmark tezts")
parser.add_argument("file", type=str, nargs="+")
args = parser.parse_args()


def _partition_tezt_log(lines: List[str]) -> Dict[str, List[str]]:
    """
    Given the following lines:

    [09:01:58.271] [client1] Tezos address added: tz1TGu6TN5GSez2ndXXeDX6LgUDvLzPLqgYV
    [09:01:58.277] [client1] exited with code 0.
    [09:01:58.436] [node1] Feb 12 10:01:58.436 - rpc: (3) receive request to /version
    [09:01:58.436] [node1] Feb 12 10:01:58.436 - rpc: (3) input media type application/json

    Returns:

    {"client1" -> [line0, line1],
     "node1"   -> [line2, line3]}
    """
    result: Dict[str, List[str]] = {}

    regexp = r"\[.*\] \[(\w+)\]"
    for line in lines:
        re_match = re.match(regexp, line)
        if not re_match:
            continue
        ident = re_match[1]
        if ident.startswith("client") or ident.startswith("node"):
            result[ident] = result.get(ident, []) + [line]

    return result


class Scenario:
    def __init__(
        self,
        name: str,
        reference_label: str,
        other_label: str,
        clients: bool,
        nodes: bool,
        total: bool,
    ):
        assert name
        self.name = name  # The name coming from json
        self.reference_label = reference_label  # The label coming from json
        self.other_label = other_label  # The label coming from json
        self.clients: bool = (
            clients  # whether to track the performances of tezos-client
        )
        self.nodes: bool = nodes  # whether to track the performances of tezos-node
        # Name (client1, node0, etc.) to list of (rpc identifier, list of timings for this RPC)
        self.reference: Dict[str, List[Tuple[str, List[float]]]] = {}
        self.other: Dict[str, List[Tuple[str, List[float]]]] = {}
        self.total = total  # Whether to draw the "total" bars

    def to_draw(self) -> Dict[draw.DrawConfig, List[draw.ToDraw]]:
        result: Dict[draw.DrawConfig, List[draw.ToDraw]] = {}

        n = self.name.replace(" ", "_")

        client_labels = draw.DrawConfig(
            f"{n}_client",
            "sum of tezos-client calls durations (bar)\nnumber of calls (atop)",
            "sum of tezos-client calls durations (milliseconds)",
            self.total,
        )
        node_labels = draw.DrawConfig(
            f"{n}_node",
            "sum of tezos-node RPC answers durations (bar)\nnumber of RPC calls (atop)",
            "sum of tezos-node RPC answers durations (milliseconds)",
            self.total,
        )

        result[client_labels] = []
        result[node_labels] = []

        mode_to_indices = {
            self.reference_label: (0, 0),
            self.other_label: (0, 0),
        }  # (node, client)

        for (is_reference, keys) in [
            (True, self.reference.keys()),
            (False, self.other.keys()),
        ]:
            mode: str = self.reference_label if is_reference else self.other_label
            for key in keys:
                print(f"{mode} key: {key}")
                is_node = key.startswith("node")
                if is_node and not self.nodes:
                    continue  # Tracking performances of nodes is not requested
                if not is_node and not self.clients:
                    continue  # Tracking performances of clients is not requested
                mode_pair_idx = 0 if is_node else 1
                pair = mode_to_indices[mode]
                idx = pair[mode_pair_idx]
                desc = "node" if is_node else "client"
                name = f"{desc}{str(idx)}"
                result_key = node_labels if is_node else client_labels
                if is_node:
                    mode_to_indices[mode] = (pair[0] + 1, pair[1])
                else:
                    mode_to_indices[mode] = (pair[0], pair[1] + 1)

                input_ = self.reference if is_reference else self.other
                value = dict(input_[key])  # copy for safety
                to_draw = draw.ToDraw(True, name, f"{mode} ({name})", value, is_node)
                result[result_key] = result.get(result_key, []) + [to_draw]

        return result


def execute_tezt(
    task: myparser.Task, s: myparser.State
) -> Dict[str, List[Tuple[str, List[float]]]]:
    cwd_prefix = s.cwd + ">" if s.cwd else ""
    cmd = [
        "dune",
        "exec",
        "tezt/tests/main.exe",
        "--",
        "--verbose",
    ]
    if file := s.tezt_file:
        cmd += ["--file", file]
    if test := s.tezt_test:
        cmd += ["--test", test]
    if s.tezt_args:
        cmd += s.tezt_args

    def quote(s: str):
        return f'"{s}"' if " " in s else s

    env = dict(os.environ)  # copy for safety
    tz_log_key = "TEZOS_LOG"
    env[tz_log_key] = "rpc->debug"
    cmd_str = " ".join([quote(atom) for atom in cmd])
    print(f"{cwd_prefix}> {tz_log_key}={env[tz_log_key]} {cmd_str}")
    res = subprocess.run(
        cmd, capture_output=True, text=True, check=False, cwd=s.cwd, env=env
    )
    if res.returncode != 0:
        print("tezt command failed", file=sys.stderr)
        for (name, stream) in [("stdout", res.stdout), ("stderr", res.stderr)]:
            if stream:
                print(f"{name} was:", file=sys.stderr)
                print(stream)
        sys.exit(1)
    lines = res.stdout.split("\n")
    partition = _partition_tezt_log(lines)
    if not partition:
        raise Exception(f"No client or node lines found. Tezt stderr was: {res.stderr}")
    partition_domain = partition.keys()
    partition_domain_str = " ".join(list(partition_domain))
    print(f"Processes found in log: {partition_domain_str}")
    result: Dict[str, List[Tuple[str, List[float]]]] = {}
    client_analyser = analyser.ClientAnalyser()
    node_analyser = analyser.NodeAnalyser()
    timelesses = []
    for ident in partition_domain:
        is_node = ident.startswith("node")
        a = node_analyser if is_node else client_analyser
        analysis = analyser.analyze_log(a, partition[ident])
        if not analysis:
            print(f"No timings found for {ident}")
            timelesses.append(ident)
            continue
        if is_node and task.nodes_filter:
            analysis = analyser.filter_analysis(analysis, task.nodes_filter)
        elif not is_node and task.clients_filter:
            analysis = analyser.filter_analysis(analysis, task.clients_filter)
        if not analysis:
            continue
        normalized = (
            rpc.normalize_many(analysis)
            if is_node
            else commands.normalize_many(analysis)
        )
        # print(f"Timings for {node_ident}:")
        # print(normalized)
        assert ident not in result
        result[ident] = normalized
    if len(timelesses) == len(partition_domain):
        domain_str: str = " ".join(partition_domain)
        raise Exception(
            f"None of the processes ({domain_str}) has a timing (task {task.name}, state {s.label})"
        )
    return result


def execute_tezts(
    order: List[str],
    task: myparser.Task,
    scenario: Scenario,
):
    assert len(order) == 2
    assert all([x in ["reference", "other"] for x in order])
    if task.skip:
        print(f'Skipping scenario "{task.name}", because it specifies "skip": true')
        return

    def checkout_make(state: myparser.State, order_member: str):
        cwd_prefix = state.cwd if state.cwd else ""
        if state.git:
            git_cmd = ["git", "checkout", state.git]
            git_cmd_str = " ".join(git_cmd)
            print(f"{cwd_prefix}> {git_cmd_str}")
            subprocess.run(git_cmd, check=True, cwd=state.cwd)
        print(f"{cwd_prefix}> make  # {order_member} state")
        subprocess.run(["make"], check=True, cwd=state.cwd)

    zero_is_ref = order[0] == "reference"
    first: myparser.State = task.reference if zero_is_ref else task.other
    second: myparser.State = task.other if zero_is_ref else task.reference

    built = False
    if not first.assume_built or (
        first.git and not git.on_branch(first.git, cwd=first.cwd)
    ):
        checkout_make(task.reference, order[0])
        built = True
    first_dict = execute_tezt(task, first)
    if order[0] == "reference":
        scenario.reference = first_dict
    else:
        scenario.other = first_dict

    if built or not second.assume_built:
        checkout_make(second, order[1])
        built = True
    second_dict = execute_tezt(task, second)
    if order[1] == "reference":
        scenario.reference = second_dict
    else:
        scenario.other = second_dict

    if built and first.assume_built:
        checkout_make(first, "restore initial")


def main() -> int:
    scenarios: List[Scenario] = []
    for task in myparser.parse(args.file):
        name = task.name
        scenario = Scenario(
            name,
            task.reference.label,
            task.other.label,
            task.clients,
            task.nodes,
            task.total,
        )
        execution_order = None
        if (
            task.reference.assume_built
            and task.reference.git
            and git.on_branch(task.reference.git, cwd=task.reference.cwd)
        ):
            execution_order = ["reference", "other"]
        elif (
            task.other.assume_built
            and task.other.git
            and git.on_branch(task.other.git, cwd=task.other.cwd)
        ):
            execution_order = ["other", "reference"]
        else:
            execution_order = ["reference", "other"]
        assert execution_order
        execute_tezts(execution_order, task, scenario)
        scenarios.append(scenario)

    for scenario in scenarios:
        d: Dict[draw.DrawConfig, List[draw.ToDraw]] = scenario.to_draw()
        for (k, v) in d.items():
            if v:
                draw.draw(k, v)

    return 0


if __name__ == "__main__":
    sys.exit(main())
