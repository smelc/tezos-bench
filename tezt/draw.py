import functools
import matplotlib.pyplot as plt  # type: ignore
import numpy as np
import os
import sys
from typing import Any, Dict, List, Optional, Set, Tuple


def _autolabel(
    ax,
    rects: List[Tuple[Any, int]],
):
    """Attach a text label above each bar in *rects*."""
    i = -1
    for (rect, nb_calls) in rects:
        i += 1
        assert nb_calls >= 0
        if nb_calls == 0:
            continue
        text = str(nb_calls)
        ax.annotate(
            text,
            xy=(rect.get_x() + rect.get_width() / 2, rect.get_height()),
            xytext=(0, 3),  # 3 points vertical offset
            textcoords="offset points",
            ha="center",
            va="bottom",
        )


# TODO @smelc: rename rpc into x_key
class ToDraw:
    def __init__(
        self,
        vanilla_or_light: bool,
        name: str,
        legend: str,
        data: Dict[str, List[float]],
        node_or_client: bool,
    ):
        """
        data: rpc name to sum of duration of this RPC
        name: node1, node2, client1, etc.
        node_or_client: whether data concerns RPCs of nodes (True)
                        or commands of tezos-client (False)
        """
        self.vanilla_or_light = vanilla_or_light
        self.name = name
        self.legend = legend
        self.data: Dict[str, List[float]] = data
        self.node_or_client = node_or_client

    def get_rpcs(self) -> Set[str]:
        return set(self.data.keys())

    def get_rpc_durations_sum(self, rpc: str, default: float = 0.0) -> float:
        durations = self.data.get(rpc, None)
        return sum(durations) if durations else default

    def get_rpc_nbcalls(self, rpc: str) -> int:
        durations = self.data.get(rpc, None)
        return len(durations) if durations else 0

    def get_total(self) -> float:
        all_durations: List[float] = []
        for rpc in self.data.keys():
            all_durations.extend(self.data[rpc])
        return sum(all_durations)

    def get_duration_sum(self, rpc_or_name: str) -> float:
        if rpc_or_name == "total":
            # the grand total
            return self.get_total()
        else:
            # an RPC
            return self.get_rpc_durations_sum(rpc_or_name)


class DrawConfig:
    def __init__(self, filename_hint: str, title: str, y_title: str, total: bool):
        self.filename_hint = filename_hint
        self.title = title
        self.y_title = y_title
        self.total = total  # Whether to show the "total" bars


# TODO @smelc: rename rpc into x_key
def draw(draw_config: DrawConfig, to_draw: List[ToDraw]):
    if to_draw:
        node_or_client = to_draw[0].node_or_client
        assert all([d.node_or_client == node_or_client for d in to_draw])
    else:
        print("Nothing to draw, weird O_o", file=sys.stderr)

    fig, ax = plt.subplots(figsize=(12, 12))  # Make figure larger
    labels_set: Set[str] = set()
    for data in to_draw:
        labels_set.update(data.get_rpcs())
    if draw_config.total:
        labels_set.update(["total"])

    def compare(rpc_or_name1: str, rpc_or_name2: str) -> int:
        """
        :rpc_or_name1: Either an RPC (starts with a slash) or the name
        of a node or a client, like node1, client1, etc. The names
        are used when drawing the bars of the total of all rpcs
        of a node
        """
        is_rpc1 = rpc_or_name1.startswith("/")
        is_rpc2 = rpc_or_name2.startswith("/")
        if is_rpc1:
            if is_rpc2:
                duration1 = sum(
                    [d.get_rpc_durations_sum(rpc_or_name1) for d in to_draw]
                )
                duration2 = sum(
                    [d.get_rpc_durations_sum(rpc_or_name2) for d in to_draw]
                )
                if duration1 < duration2:
                    return -1
                elif duration2 > duration1:
                    return 1
                return 0
            else:
                # not is_rpc2, rpc_or_name1 goes to the right
                return -1
        else:
            if is_rpc2:
                # not is_rpc1, rpc_or_name2 goes to the right
                return 1
            else:
                if rpc_or_name1 < rpc_or_name2:
                    return -1
                elif rpc_or_name2 > rpc_or_name2:
                    return 1
                return 0

    labels = sorted(list(labels_set), key=functools.cmp_to_key(compare))

    x, width = np.arange(len(labels)), 0.5
    rects_and_nbcalls: List[Tuple[Any, int]] = []
    i = 0
    for data in to_draw:
        durations = [data.get_duration_sum(rpc) for rpc in labels]
        ratio = 0.6  # found by trial and error
        custom_x = [v + (i * (width * ratio)) for v in x]
        rects = list(
            ax.bar(
                x=custom_x,
                height=durations,
                width=width * ratio,
                label=data.legend,
            )
        )
        nb_calls = [data.get_rpc_nbcalls(rpc) for rpc in labels]
        assert len(rects) == len(nb_calls)
        for j in range(0, len(rects)):
            pair = (rects[j], nb_calls[j])
            rects_and_nbcalls.append(pair)
        i += 1

    ax.set_ylabel(draw_config.y_title)
    ax.set_title(draw_config.title)
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation=90)
    ax.legend()

    _autolabel(ax, rects_and_nbcalls)

    fig.tight_layout()
    plt.subplots_adjust(bottom=0.4)  # Make bottom area larger

    i = 0
    while True:
        img_filepath = f"{draw_config.filename_hint}_{i}.png"
        if not os.path.isfile(img_filepath):
            break  # Found filename
        i += 1

    plt.savefig(img_filepath)
    print(f"Written {img_filepath}")
    plt.close()
