""" Module to perform git queries """

import subprocess
from typing import List


def _get_cmd_stdout(cmd: List[str], cwd=None) -> str:
    cmd_str = " ".join(cmd)
    cwd_str = cwd if cwd else ""
    print(f"{cwd_str}> {cmd_str}")
    res = subprocess.run(cmd, check=False, cwd=cwd, capture_output=True, text=True)
    return res.stdout


def get_branch(cwd=None) -> str:
    cmd = ["git", "branch", "--show-current"]
    return _get_cmd_stdout(cmd, cwd=cwd).strip()


def get_changeset(cwd=None) -> str:
    cmd = ["git", "rev-parse", "HEAD"]
    res = _get_cmd_stdout(cmd, cwd=cwd).strip()
    res = res[:7] if res else res
    return res


def get_commit_title(commit="HEAD", cwd=None):
    cmd = ["git", "log", "--format=%B", "-n", "1", commit]
    message = _get_cmd_stdout(cmd, cwd=cwd)
    if not message:
        raise Exception(f"Cannot get commit title of commit {commit}")
    message_lines = message.split("\n")
    if not message_lines:
        raise Exception(f"title of commit {commit} is empty")
    return message_lines[0]


def on_branch(branch: str, cwd=None) -> bool:
    return get_branch(cwd=cwd) == branch
