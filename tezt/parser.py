""" Module parsing the files given to main.py """

import json
import os

import git

from typing import Any, Dict, List, Optional


class State:
    """ A state on which tezt should be execute """

    def __init__(
        self,
        label: str,
        assume_built: Optional[bool],
        cwd: Optional[str],
        git: Optional[str] = None,
        tezt_args: Optional[List[str]] = None,
        tezt_file: Optional[str] = None,
        tezt_test: Optional[str] = None,
    ):
        self.label = label
        if assume_built is None:
            # We assume tezos is built if it's not required to
            # change branch to execute the scenario
            self.assume_built = True if git is None else False
        else:
            self.assume_built = assume_built
        if cwd:
            if not os.path.isdir(cwd):
                raise Exception(
                    f'"cwd" field should denote a directory but {cwd} isn\'t'
                )
        self.cwd = cwd
        self.git = git
        if not tezt_file and not tezt_test:
            raise Exception('One of "tezt_file" or "tezt_test" must be specified')
        self.tezt_args = tezt_args if tezt_args else []
        self.tezt_file = tezt_file
        self.tezt_test = tezt_test


class Task:
    """ A task: how to compare two states """

    def __init__(
        self,
        path: str,
        name: str,
        reference: State,
        other: State,
        clients: bool = True,
        clients_filter: Optional[str] = None,
        nodes: bool = True,
        nodes_filter: Optional[str] = None,
        skip: Optional[bool] = None,
        total: bool = False,
    ):
        self.path: str = path  # The path of the file from which the task was created
        self.name: str = name  # The task's name
        self.reference: State = reference
        self.other: State = other
        self.clients: bool = clients  # Whether to track performance of tezos-client
        self.clients_filter: Optional[
            str
        ] = clients_filter  # An optional re to filter commands considered
        self.nodes: bool = nodes  # Whether to track performance of tezos-node
        self.nodes_filter: Optional[
            str
        ] = nodes_filter  # An optional re to filter rpcs considered
        self.skip: bool = skip if skip else False
        self.total: bool = total  # Whether to draw the "total" bars


def _get_default_label(cwd=None):
    def _ellipsis(s: str, i: int):
        if not s:
            return s
        return s if len(s) <= i else s[:i] + "…"

    title = git.get_commit_title(cwd=cwd)
    title = _ellipsis(title, 48)
    changeset = git.get_changeset(cwd=cwd)
    changeset = _ellipsis(changeset, 7)
    branch = git.get_branch(cwd=cwd)
    branch = _ellipsis(branch, 32)
    return f"{title}@{branch} - {changeset}"


def _parse_state(file: str, d: Dict[str, Any]) -> State:
    """
    :file: The file from which 'd' was obtained
    :d: A dictionary obtained from such json:
    {
        "tezt_test": "heavy_duty vanilla (alpha)"
    }

    or

    {
        "tezt_file": "RPC_test.ml"
    }
    """
    cwd = d.get("cwd", None)
    label = d.get("label", None)
    label = label if label else _get_default_label(cwd)
    return State(
        assume_built=d.get("assume_built", None),
        cwd=cwd,
        git=d.get("git", None),
        label=label,
        tezt_args=d.get("tezt_args", []),
        tezt_file=d.get("tezt_file", None),
        tezt_test=d.get("tezt_test", None),
    )


def _parse_one_dict(file: str, d: Dict[str, Any]) -> Task:
    """
        :file: The file from which 'd' was obtained
        :d: A dictionary obtained from such json:

    {
        "name": "heavy duty",
        "reference": {
            "tezt_test": "heavy_duty vanilla (alpha)"
        },
        "other": {
            "tezt_file": "heavy_duty light (alpha)",
            "label": "foobar",
            "cwd": "/home/smelc/dev/tezos"
        },
        "clients": true,
        "clients_filter": "rpc .*",
        "nodes": true
    }
    """
    for field in ["name", "reference", "other"]:
        if field not in d.keys():
            raise Exception(f"Json task misses string field '{field}'")
    return Task(
        file,
        d["name"],
        _parse_state(file, d["reference"]),
        _parse_state(file, d["other"]),
        clients=bool(d.get("clients", True)),
        clients_filter=d.get("clients_filter", None),
        nodes=bool(d.get("nodes", True)),
        nodes_filter=d.get("nodes_filter", None),
        skip=d.get("skip", None),
        total=d.get("total", False),
    )


def _parse_one_file(file: str) -> List[Task]:
    with open(file, "r") as handle:
        json_tasks = json.load(handle)
        return [_parse_one_dict(file, t) for t in json_tasks]


def parse(files: List[str]) -> List[Task]:
    result = []
    for f in files:
        result += _parse_one_file(f)
    return result
