import re
from typing import Dict, List, Tuple, Any


def normalize(rpc: str) -> str:
    """
    Normalize RPC paths, to merge data of specific RPC calls; so
    that they are gathered in the same x data
    """
    if re.match(r"/chains/\w+/blocks$", rpc):
        return "/chains/<chain_id>/blocks"
    if re.match(r"/chains/\w+/chain_id$", rpc):
        return "/chains/<chain_id>/chain_id"
    if re.match(r"/describe/.*", rpc):
        return "/describe"
    for suffix in [
        "mempool/monitor_operations",
        "mempool/pending_operations",
        "mempool/request_operations",
    ]:
        if re.match(r"/chains/\w+/" + suffix, rpc):
            return "/chains/<chain_id>/" + suffix
    for suffix in [
        "context/bakers",
        "context/big_maps",
        "context/constants",
        "context/contracts",
        "context/delegates",
        "context/merkle_tree",
        "context/raw/bytes",
        "hash",
        "header",
        "header/shell",
        "helpers/current_level",
        "helpers/forge/operations",
        "helpers/endorsing_rights",
        "helpers/levels_in_current_cycle",
        "helpers/preapply/block",
        "helpers/preapply/operations",
        "helpers/scripts/run_operation",
        "helpers/scripts/run_operations",
        "live_blocks",
        "metadata_hash",
        "minimal_valid_time",
        "operations_metadata_hash",
        "protocols",
        "votes/ballots",
        "votes/ballot_list",
        "votes/current_period",
        "votes/current_proposal",
        "votes/current_quorum",
        "votes/listings",
        "votes/proposals",
        "votes/successor_period",
        "votes/total_voting_power",
    ]:
        if re.match(r"/chains/\w+/blocks/\w+/" + suffix, rpc):
            return "/chains/<chain_id>/blocks/<block_id>/" + suffix
    if re.match(r"/chains/\w+/blocks/\w+/context/bakers/\w+", rpc):
        return "/chains/<chain_id>/blocks/<block_id>/context/bakers"
    if re.match(r"/chains/\w+/blocks/\w+/context/nonces/\d+", rpc):
        return "/chains/<chain_id>/blocks/<block_id>/context/nonces"
    if re.match(r"/chains/\w+/blocks/\w+/helpers/baking_rights.*", rpc):
        return "/chains/<chain_id>/blocks/<block_id>/helpers/baking_rights"
    if re.match(r"/network/points/\w+", rpc):
        return "/network/points"
    if re.match(r"/injection/block.*", rpc):
        return "/injection/block"
    if re.match(r"/injection/operation.*", rpc):
        return "/injection/operation"
    normals = [
        "/chains/main/mempool/pending_operations",
        "/config/network/user_activated_upgrades",
        "/describe/network/version",
        "/monitor/bootstrapped",
        "/monitor/heads/main",
        "/network/version",
        "/version",
        "/protocols",
    ]
    if rpc in normals:
        # Expected not to be affected
        return rpc
    raise Exception(
        f"Unexpected normal rpc in rpc.py's normalize function: {rpc}. Add it to normals if normal already, or extend the matchers above."
    )


def normalize_many(d) -> List[Tuple[str, Any]]:
    """
    Applies _normalize_rpc to keys. Values can be anything.
    """
    result: Dict[str, Any] = {}
    assert isinstance(d, dict) or isinstance(d, list)
    for (k, v) in d.items() if isinstance(d, dict) else d:
        assert k
        assert v
        k_norm = normalize(k)
        assert k_norm
        result[k_norm] = result.get(k_norm, []) + v
    return list(result.items())
