from typing import Any, Dict, List, Match, Optional, Tuple

import datetime
import re
import sys


class Analyser:
    def __init__(self, start_re: str, end_re: str):
        """
        :start_re: How to identify starting lines of calls we wanna
                   track the duration
        :end_re: How to identify ending lines of a call we wanna
                 track the duration
        """
        self.start_re = start_re
        self.end_re = end_re

    def parse_start(self, match: Match):
        raise Exception("parse_start should be implemented in subclasses")

    def parse_end(self, match: Match) -> Tuple[float, str]:
        raise Exception("parse_end should be implemented in subclasses")


class ClientAnalyser(Analyser):

    # Format to match for example "16:45:05.847"
    # Created by tezt
    date_format = "%H:%M:%S.%f"

    def __init__(self):
        Analyser.__init__(
            self,
            r"^\[(\S+)\] \[client\d+\] \./tezos-(?:admin-)?client (.*)",
            r"^\[(\S+)\] \[client\d+\] exited with code (\d+)",  # Needs a patch in tezt for this line to be printed
        )
        self.opened: Optional[Tuple[datetime.datetime, str]] = None

    def parse_start(self, match: Match):
        date_str = match[1]
        cmd = match[2]
        if self.opened:
            raise Exception(
                f"Reentrant client log: ({date_str}, {cmd}). The following pair is opened already: ({str(self.opened[0])}, {str(self.opened[1])})."
            )
        start_date = datetime.datetime.strptime(date_str, ClientAnalyser.date_format)
        self.opened = (start_date, cmd)

    def parse_end(self, match: Match) -> Tuple[float, str]:
        if not self.opened:
            raise Exception("No client log being parsed")
        start_date = self.opened[0]
        date_str = match[1]
        end_date = datetime.datetime.strptime(date_str, ClientAnalyser.date_format)
        duration = (end_date - start_date).microseconds / 1000
        result = (duration, self.opened[1])
        self.opened = None  # prepare next match
        return result


class NodeAnalyser(Analyser):
    # Format to match for example "Aug 21 10:56:31.925"
    # Of this form, because caused by TEZOS_LOG being "rpc->debug"
    date_format = "%b %d %H:%M:%S.%f"

    def __init__(self):
        Analyser.__init__(
            self,
            r"^\[\S+\] \[\S+\] (.*) - rpc: \((\w+)\) received request (.*)",
            r"\[\S+\] \[\S+\] (.*) - rpc: connection closed (\w+)",
        )
        # RPCs whose starting line has been parsed, but the endline hasn't been
        # yet. The key is the request number while the value is a pair of a
        # datetime and the RPC being called.
        self.opened: Dict[int, Tuple[datetime.datetime, str]] = {}

    def parse_start(self, match: Match):
        date_str = match[1]
        request = int(match[2])
        rpc = match[3]
        if request in self.opened:
            raise Exception(f"RPC request {request} started twice")
        start_date = datetime.datetime.strptime(date_str, NodeAnalyser.date_format)
        self.opened[request] = (start_date, rpc)

    def parse_end(self, match: Match) -> Tuple[float, str]:
        date_str = match[1]
        request = int(match[2])
        if request not in self.opened:
            raise Exception(f"RPC request {request} ended before having started")
        (start_date, rpc) = self.opened.pop(request)
        end_date = datetime.datetime.strptime(date_str, NodeAnalyser.date_format)
        duration = (end_date - start_date).microseconds / 1000
        return (duration, rpc)


def analyze_log(
    analyser: Analyser, lines: List[str], debug: bool = False
) -> List[Tuple[str, List[float]]]:
    """
    Given a list of lines, returns a list of pairs. The pair's
    first member is the rpc request. The pair's second member
    is a list of duration of this request; in milliseconds.
    """

    def log(msg: str):
        if debug:
            print(msg)

    result: Dict[str, List[float]] = {}
    for line in [line.strip() for line in lines]:
        match_start = re.match(analyser.start_re, line)
        if match_start:
            # print(f"Opening: {line}")
            analyser.parse_start(match_start)
            continue
        match_end = re.match(analyser.end_re, line)
        if match_end:
            # print(f"Closing: {line}")
            (duration, ident) = analyser.parse_end(match_end)
            result[ident] = result.get(ident, []) + [duration]
    for k in result.keys():
        result[k].sort()
    result_list = list(result.items())
    result_list.sort(key=lambda p: p[0])
    for (k, v) in result_list:
        vs = " ".join([str(x) for x in v])
        log(f"{k} -> {vs}")
    return result_list


def filter_analysis(
    analysis: List[Tuple[str, Any]], re_key: str
) -> List[Tuple[str, Any]]:
    """ Filters `analysis` by keeping only the keys matched by `re_key` """
    assert re_key
    res = [(k, v) for (k, v) in analysis if re.match(re_key, k)]
    if not res:
        print(f"⚠️ all keys have been filtered by regexp: {re_key} ⚠️")
        print("Is your regexp wrong?")
        print("For the record, here's the unfiltered list of keys (separated by |):")
        print("|".join([p[0] for p in analysis]))
    return res
